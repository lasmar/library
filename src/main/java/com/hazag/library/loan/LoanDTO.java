package com.hazag.library.loan;

import com.hazag.library.book.BookDTO;
import com.hazag.library.customer.CustomerDTO;
import io.swagger.annotations.ApiModel;

import java.time.LocalDate;

@ApiModel("Loan Model")
public class LoanDTO {

    private CustomerDTO customerDTO ;

    private BookDTO bookDTO ;

    private LocalDate loanbeginDate;

    private LocalDate loanendDate;

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public BookDTO getBookDTO() {
        return bookDTO;
    }

    public void setBookDTO(BookDTO bookDTO) {
        this.bookDTO = bookDTO;
    }

    public LocalDate getLoanbeginDate() {
        return loanbeginDate;
    }

    public void setLoanbeginDate(LocalDate loanbeginDate) {
        this.loanbeginDate = loanbeginDate;
    }

    public LocalDate getLoanendDate() {
        return loanendDate;
    }

    public void setLoanendDate(LocalDate loanendDate) {
        this.loanendDate = loanendDate;
    }
}
