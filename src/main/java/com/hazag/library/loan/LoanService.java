package com.hazag.library.loan;

import java.time.LocalDate;
import java.util.List;

public interface LoanService {

    Loan checkIfExist(SimpleLoanDTO simpleLoanDTO);
    Loan saveLoan(Loan loan);
    List<Loan> findLoansByStatusIsOpenAndDateBeforeDateJour(LocalDate localDate , LoanStatus loanStatus);
}
