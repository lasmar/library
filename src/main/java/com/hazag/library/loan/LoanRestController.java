package com.hazag.library.loan;

import ch.qos.logback.core.boolex.EvaluationException;
import com.hazag.library.book.Book;
import com.hazag.library.customer.Customer;
import com.hazag.library.customer.CustomerServiceImp;
import com.hazag.library.customer.MailDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rest/loan/api")
@Api(value = "Loan rest controller : contains all operation for managing Loan book " )
public class LoanRestController {

    @Autowired
    private LoanServiceImpl loanServiceImp;

    @Autowired
    private CustomerServiceImp customerServiceImp ;

    @PostMapping("/addNewLoan")
    @ApiOperation(value = "Create New Loan book" , response = Boolean.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201 , message = "Created : the loan is successful created"),
            @ApiResponse(code = 304 , message = "Not Modified : the loan is unsuccessufully inserted")
    })
    public ResponseEntity<Boolean> createNewLoan(@RequestBody SimpleLoanDTO simpleLoanDTO){

       // Loan loan = loanServiceImp.checkIfExist(simpleLoanDTO);
       Loan  loan = loanServiceImp.saveLoan(mapSimpleLoanDTOToLoan(simpleLoanDTO));

       if(loan != null){
           return new ResponseEntity<>(Boolean.TRUE , HttpStatus.CREATED);
       }
       return  new ResponseEntity<>(Boolean.FALSE , HttpStatus.NOT_MODIFIED);

    }


    @GetMapping("/sendMailToCustomerBeforeOneDay")
    @ApiResponses(value = {
           @ApiResponse(code = 404 , message = "NO_CONTENT : faild to"),
            @ApiResponse(code = 204 , message = "No Content : nos resultat found")
    })
    @Scheduled(cron = "0 45 22 * * ?2020")
    public ResponseEntity<List<SimpleLoanDTO>> sendMailToListCustomerWhoHaveAbookReturnDateBeforeOneDay(){

        List<Loan> loans = loanServiceImp.findLoansByStatusIsOpenAndDateBeforeDateJour(LocalDate.now().plusDays(1), LoanStatus.OPEN);

        if(CollectionUtils.isEmpty(loans)){
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

      List<SimpleLoanDTO> simpleLoanDTOS = loans.stream().map(loan -> {
            return mapLoanToSimpleLoanDTO(loan);
        }).collect(Collectors.toList());

        MailDTO mailDTO = new MailDTO();
        mailDTO.setEmailContent("The book loan date expires tomorrow . Please Return the book tomorrow afternoon at the latest");
        mailDTO.setEmailSubject("Alerte to Return book");
        loans.forEach(loan -> {
            mailDTO.setCustomerId(loan.getPk().getCustomer().getId());
            customerServiceImp.sendMailToCustomerService(mailDTO);
        });

      return new ResponseEntity<List<SimpleLoanDTO>>(simpleLoanDTOS , HttpStatus.OK);
    }



   private  Loan mapSimpleLoanDTOToLoan(SimpleLoanDTO simpleloanDTO){
        Loan loan = new Loan();
        Book book = new Book();
        Customer customer = new Customer();
        book.setId(simpleloanDTO.getBookId());
        customer.setId(simpleloanDTO.getCustomerId());
        LoanId loanId = new LoanId();
        loanId.setBook(book);
        loanId.setCustomer(customer);
        loan.setPk(loanId);
        loan.setBeginDate(simpleloanDTO.getLoanbeginDate());
        loan.setEndDate(simpleloanDTO.getLoanendDate());
        loan.setStatus(LoanStatus.OPEN);
        return loan ;
    }
  private SimpleLoanDTO mapLoanToSimpleLoanDTO(Loan loan){
        SimpleLoanDTO simpleLoanDTO = new SimpleLoanDTO();
        simpleLoanDTO.setBookId(loan.getPk().getBook().getId());
        simpleLoanDTO.setCustomerId(loan.getPk().getCustomer().getId());
        simpleLoanDTO.setLoanbeginDate(loan.getBeginDate());
        simpleLoanDTO.setLoanendDate(loan.getEndDate());

        return simpleLoanDTO ;
  }
}
