package com.hazag.library.category;

import com.hazag.library.utils.RequestPaginationDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rest/Category/api")
public class CategoryRestController  {

    @Autowired
    private CategoryServiceImp categoryServiceImp ;

    @Autowired
    ModelMapper modelMapper ;

    @PostMapping("/addCategory")
    @ApiOperation("Add new Category in the Library")
    @ApiResponses(value = {
            @ApiResponse(code = 409 , message = "Conflit : the category already exists"),
            @ApiResponse(code = 201 , message = "Created :the category is successfully inseted"),
            @ApiResponse(code = 304 , message = "Not Modified :the category is unsuccessfully inseted"),
    })
    public ResponseEntity<CategoryDTO> createNewCategory(@RequestBody CategoryDTO categoryDTORequest){

        //check is exists
       Category categoryExisting = categoryServiceImp.checkexiting(categoryDTORequest.getCode());
       if(categoryExisting != null){
           return new ResponseEntity<>(HttpStatus.CONFLICT);
       }
        Category categoryRequest = mapCategoryDTOTOCategory(categoryDTORequest);
        Category category = categoryServiceImp.saveCategory(categoryRequest);
        if(category != null && category.getCode() !=null){
        CategoryDTO categoryDTO = mapCategoryToCategoryDTO(category) ;
        return new ResponseEntity<CategoryDTO>(categoryDTO , HttpStatus.CREATED);
        }
      return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);

    }

    @PutMapping("/updateCategory")
    @ApiOperation(value= "Update Category existing in the Library" , response = CategoryDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404 ,message = "NOT FOUND : this Category not exist" ) ,
            @ApiResponse(code =200 , message = "OK :the Category is successfully update"),
            @ApiResponse(code = 304 , message = "Not MODIFIED : the category is unsuccessfuly update  ")
    })
    public ResponseEntity<CategoryDTO> updateCategory(@RequestBody CategoryDTO categoryDTORequest){

        Category categoryExisting = categoryServiceImp.checkexiting(categoryDTORequest.getCode());

        if(categoryExisting == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Category categoryRequest =  mapCategoryDTOTOCategory(categoryDTORequest);
        Category category = categoryServiceImp.saveCategory(categoryRequest);
        if(category != null){
            CategoryDTO categoryDTO = mapCategoryToCategoryDTO(category);
            return new ResponseEntity<CategoryDTO>(categoryDTO , HttpStatus.OK);
        }
        return  new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @PostMapping("/getAllCategory")
    @ApiOperation(value = "get All Categories" , response = List.class)
    @ApiResponses(value= {
            @ApiResponse(code= 200 , message = "Ok : successfull get list "),
            @ApiResponse(code = 204 , message = "No Content : no resultat found")
    })
   public ResponseEntity<List<CategoryDTO>> getAllBookCategory(@RequestBody RequestPaginationDTO requestPaginationDTO){

        List<Category> allCategory = categoryServiceImp.getAllCategory(requestPaginationDTO.getPageNo(),
                requestPaginationDTO.getPageSize(),
                requestPaginationDTO.getSortBy());

        if(!CollectionUtils.isEmpty(allCategory)) {
            List<CategoryDTO> categoryDTOS = allCategory.stream().map(category -> {
                CategoryDTO categoryDTO = mapCategoryToCategoryDTO(category);
                return categoryDTO;
            }).collect(Collectors.toList());
            return new ResponseEntity<List<CategoryDTO>>(categoryDTOS , HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
   }


     Category mapCategoryDTOTOCategory(CategoryDTO categoryDTO){
         return modelMapper.map(categoryDTO , Category.class);
     }
     CategoryDTO mapCategoryToCategoryDTO(Category category){
        return modelMapper.map(category , CategoryDTO.class);
     }
}