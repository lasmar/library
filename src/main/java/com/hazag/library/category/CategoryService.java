package com.hazag.library.category;

import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

   Category checkexiting(String code);
   Category saveCategory(Category category);
   List<Category> getAllCategory(Integer pageNo , Integer pagesize , String sort);
}
