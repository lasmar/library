package com.hazag.library.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("category")
public class CategoryServiceImp implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository ;

    @Override
    public Category checkexiting(String code) {

      Optional<Category> category =  categoryRepository.findByCode(code);
      if(category.isPresent()) return category.get();
      return null;
    }

    @Override
    public Category saveCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> getAllCategory(Integer pageNo , Integer pageSize , String sortBy) {
        Pageable paging = PageRequest.of(pageNo , pageSize , Sort.by(sortBy).descending());
       Page<Category> categoys = categoryRepository.findAll(paging);
       if(categoys.hasContent())
           return categoys.getContent();
        return  new ArrayList<Category>();
    }
}
