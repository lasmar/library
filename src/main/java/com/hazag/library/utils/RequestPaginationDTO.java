package com.hazag.library.utils;

public class RequestPaginationDTO {

    private Integer pageNo ;
    private Integer pageSize ;
    private String sortBy ;

    public RequestPaginationDTO() {
    }

    public RequestPaginationDTO(Integer pageNo, Integer pageSize, String sortBy) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.sortBy = sortBy;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }
}
