package com.hazag.library.customer;

import com.hazag.library.loan.Loan;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table (name = "CUSTOMER")
@Getter
@Setter
public class Customer implements Serializable {
    @Id
    @Column (name="CUSTOMER_ID")
    private Integer id;
    @Column(name= "FIRST_NAME")
    private String firstName;
    @Column (name = "LAST_NAME" )
    private String lastName;
    @Column(name = "JOB")
    private String job;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "EMAIL" , unique = true)
    private String email;
    @Column(name = "CREATION_DATE")
    private LocalDate creationDate;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.customer", cascade = CascadeType.ALL)
    public Set<Loan> loans ;
}
