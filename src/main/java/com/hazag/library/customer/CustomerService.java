package com.hazag.library.customer;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


public interface CustomerService {

    Customer checkIsExist(Integer id);
    Customer saveCustomer(Customer customer);
    Customer findCustomerById(Integer id);
}
