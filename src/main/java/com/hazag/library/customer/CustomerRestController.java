package com.hazag.library.customer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.SpelQueryContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;

@RestController
@RequestMapping("/rest/customer/api")
@Api(value = "Customer Rest Controller : contains all operation for managing Customer ")
@CrossOrigin()
public class CustomerRestController {

    public static final Logger LOGGER = LoggerFactory.getLogger(CustomerRestController.class);

    @Autowired
    private ModelMapper modelMapper ;

    @Autowired
    private CustomerServiceImp customerServiceImp ;



    @PostMapping("/addCustomer")
    @ApiOperation(value = "Add new Customer " , response = CustomerDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201 , message = "Created : successfull "),
            @ApiResponse(code = 304  , message = "Not Modified : unseccfull modified"),
            @ApiResponse(code = 409 , message = "CONFLIT : the customer already exist")
    })
    public ResponseEntity<CustomerDTO> createNewCustomer(@RequestBody CustomerDTO customerDTO) {

        Customer cutomerExisting = customerServiceImp.checkIsExist(customerDTO.getId());

        if (cutomerExisting == null) {
            Customer custumerRequest = customerServiceImp.saveCustomer(mapCustomerDTOToCustomer(customerDTO));

            if (custumerRequest != null) {
                CustomerDTO customerDto = mapCustomerToCustomerDTO(custumerRequest);
                return new ResponseEntity<CustomerDTO>(customerDTO , HttpStatus.CREATED);
            }
            return  new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping("/sendMailToCustomer")
    @ApiResponses(value = {
            @ApiResponse(code = 404 , message = "NOT FOUND : no customer found , or wrong email")
    })
    public ResponseEntity<Boolean> sendMailToCustomer(@RequestBody MailDTO loanMailDTO){
            return  customerServiceImp.sendMailToCustomerService(loanMailDTO);
    }


    private Customer mapCustomerDTOToCustomer(CustomerDTO customerDTO){
        Customer customer = modelMapper.map(customerDTO , Customer.class);
        customer.setCreationDate(LocalDate.now());
        return customer ;
    }
    private CustomerDTO mapCustomerToCustomerDTO(Customer customer){
        return modelMapper.map(customer , CustomerDTO.class);
    }
}
