package com.hazag.library.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service("bookService")
@Transactional
public class BookServiceImpl implements BookService {

    private String s ;
    @Autowired
    private BookRepository bookRepository ;

    @Override
    public Book checkExisting(String isbn){

        Optional<Book> book = bookRepository.findByIsbn(isbn);
        if(book.isPresent()) return book.get();

        return null ;
    }

    @Override
    public Book saveBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public boolean checkIdExist(Integer id) {
        return bookRepository.existsById(id);
    }

    @Override
    public void deleteBook(Integer bookid) {
        bookRepository.deleteById(bookid);
    }

    @Override
    public List<Book> getBooksByCategory(String codeCategory) {
        return bookRepository.getBooksByCategory(codeCategory);
    }

}
