package com.hazag.library.book;

import com.hazag.library.category.Category;
import com.hazag.library.category.CategoryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import java.time.LocalDate;

@ApiModel("Book Model")
public class BookDTO {

    @ApiModelProperty(value = "Book id")
    private Integer id ;

    @ApiModelProperty(value = "Book title")
    private String title ;

    @ApiModelProperty(value = "Book isbn")
    private String isbn ;

    @ApiModelProperty(value = "Date Relase")
    private LocalDate relaseDate ;

    @ApiModelProperty(value = "Date Register")
    private LocalDate registerDate;

    @ApiModelProperty(value = "Total Exemplaire")
    private Integer totalExemplaireS;

    @ApiModelProperty(value = "Author")
    private String author ;

    @ApiModelProperty(value = "Category")
    private CategoryDTO categoryDTO ;

    public BookDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public LocalDate getRelaseDate() {
        return relaseDate;
    }

    public void setRelaseDate(LocalDate relaseDate) {
        this.relaseDate = relaseDate;
    }

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }

    public Integer getTotalExemplaireS() {
        return totalExemplaireS;
    }

    public void setTotalExemplaireS(Integer totalExemplaireS) {
        this.totalExemplaireS = totalExemplaireS;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public CategoryDTO getCategoryDTO() {
        return categoryDTO;
    }

    public void setCategoryDTO(CategoryDTO categoryDTO) {
        this.categoryDTO = categoryDTO;
    }
}
